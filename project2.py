from matplotlib import pyplot as plt
import numpy as np
import matplotlib.cm as cm
import time
from math import exp

#defining our polynomial fit for the number of deer seen vs. time it takes to run code
de_points = [102,	102,	0,	120,	97,	30,	54,	53,	55,	60,	49,	53,	60,	9,	23,	129,	99,	120,	0,	80,	97,	105,	93]
time_points = [138.645081996917,	196.021453142166,	48.5213570594787,	120.753373146057,	155.704461097717,	74.0021140575408,	68.4748349189758,	68.85236287117,	79.5778641700744,	96.6217899322509,	86.0358109474182,	101.453033924102,	105.369988918304,	105.759491920471,	111.228491067886,	165.03674006462,	154.279716014862,	194.928621053695,	97.6073129177093,	148.90164899826,	114.029828071594,	162.697911024093,	122.3893869]
t = np.arange(0.0, 200.0, 0.01)
s = (0.0065*(t**2) - (0.0368*t) + 80.225)
r = (0.7856*t + 63.82)
#u =  (68.773*(exp**(0.007*t))) #not working : ( 




class Car():
	
	def __init__(self, x=0, dx=5, bumper = None):
		self.x = x
		self.bumper = bumper # keeps track of the car in front of it 
		self.dx = dx

	def move(self):
		self.x = self.x + self.dx 
	
	def change_dx(self, dx):
		self.dx = dx 

	def speedUp(self):
		self.dx = 5 # car starts travelling at the normal rate of traffic again 
                
	def seeDeer(self, slow, deerSeen): 
		if np.random.randint(0,10) > 8 and deerSeen == False: # probability of a car seeing a deer 
			self.dx = 1
		


CarNumber = 1000 # total number of cars 
car_list = [] # an array of all the cars
car_pos = [] # position of all the cars x position for plotting 
graph_y = [] # position of all the cars y positions for plotting 
xDeer = [] # x position of a deer seen for plotting 
yDeer = [] # y position of the deer for plotting 
carInGraph = []

for i in range(0,CarNumber): # initialising 100 cars 
	if i == 0:
                
                # the first car starts at x = 0, with dx = 5 and is following no one
		new_car = Car(15, 5, None)  
		car_list.append(new_car)
	else:
                # all the cars behind the first car, each following the car ahead of it  
		new_car = Car(car_list[i-1].x - 10, car_list[i-1].dx, car_list[i-1]) 
		car_list.append(new_car)

        # adding each car's x position to the array 
	car_pos.append(new_car.x)
        
        # plotting everyone on the y = 20 line 
	graph_y.append(20)

# all cars initially don't see a deer 
deerSeen = False

# keeping track of the time 
t0 = time.time()

# running a loop till the last car leaves the highway 
while car_list[len(car_list) -1].x < 100 :

	print ("pos of last car: ", car_list[len(car_list) -1].x)

        # the arrays reset so the plots keep changing (for animation purposes) 
	del car_pos[:]
	del graph_y[:]
	del xDeer [:]
	del yDeer [:]
	del carInGraph [:]
	#carselect = 3

        # makes the cars return to their normal speed after a few steps 
        if car_list[len(car_list) -1].x % 5 == 0:
                deerSeen = False

        # for each of the 1000 cars...  
	for i in range(0, CarNumber):
		if car_list[i].x < 100:
			if car_list[i].x > 0:
				carInGraph.append(i)
				
	for i in range(0,CarNumber):

                # for each car in a certain x position
		if car_list[len(car_list) -1].x % 10 == 0:

                        # a random car gets selected 
			number = np.random.randint(0, len(carInGraph))
			carselect = carInGraph[number]
			#carselect = np.random.randint(0,(CarNumber - 1))

                        # makes sure the car that was selected is in our highway zone 
                        
			#while car_list[carselect].x > 100 and car_list[carselect].x < 0 :
				#carselect = np.random.randint(0,(CarNumber - 1))

                        # if the car is inside the highway zone that we can see 
                
			if car_list[carselect].x < 1000 and car_list[carselect].x > 0 :

                                # makes the selected car see the deer 

				if i == carselect:
					car_list[i].seeDeer(1,deerSeen)
					deerSeen = True
					xDeer.append(car_list[carselect].x)
					yDeer.append(20)
					print("Hi deer")
					#print carselect

                                # makes all the cars behind carselect slow down too 
                                
			        if i > carselect:
					car_list[i].change_dx(car_list[i-1].dx)

                        # if the car has not seen the deer, goes back to normal speed of traffic 

			if deerSeen == False:
					car_list[i].speedUp()

		car_list[i].move()
		car_pos.append(car_list[i].x)
		graph_y.append(20)

        #plots everything 

	
	plt.figure(1)
	colours = cm.rainbow(np.linspace(0,1,len(graph_y)))
	plt.title("Highway to Deer")	
	plt.scatter(car_pos,graph_y,color = colours)
	plt.plot(xDeer, yDeer, 'rx', mew=10, ms=20)
	plt.axis([0, 100, 10, 30])
	plt.draw()
	plt.pause(.0001)
	plt.clf()
	
	colours = (np.linspace(0,1,len(graph_y)))
	#counter plot for deer seen, just maps where the "almost hits" occur
	plt.figure(2)
	plt.title("DeerSeen Tracker")
	plt.plot(xDeer, yDeer, 'ro')
	#plot of deer seen vs time it take to run the code
	plt.figure(3)
	plt.title("Number of Deer Seen vs Time it Takes for Code to Run (2nd Order Polynomial Fit)")
	plt.scatter(de_points,time_points)
	plt.xlabel("Number of Deer Seen")
	plt.ylabel("Time")
	plt.annotate('R^2 Value= 0.62981 ', xy=(200, 1), xytext=(0,200))
	plt.plot(t, s, lw=2)
	#plot of deer seen vs time it take to run the code 
	plt.figure(4)
	plt.title("Number of Deer Seen vs Time it Takes for Code to Run Linear Fit")
	plt.scatter(de_points,time_points)
	plt.xlabel("Number of Deer Seen")
	plt.ylabel("Time")
	plt.annotate('R^2 Value= 0.57746 ', xy=(200, 1), xytext=(0,200))
	plt.plot(t, r, lw=2)
	#plot of deer seen vs time it take to run the code expodentail fit
	#plt.figure(5)
	#plt.scatter(de_points,time_points)
	#plt.xlabel("Number of Deer Seen")
	#plt.ylabel("Time")
	#plt.annotate('R^2 Value= 0.57282 ', xy=(200, 1), xytext=(0,200))
	#plt.plot(t, u, lw=2)

# total time taken for the simulation

t1 = time.time()	
print("time elapsed", t1-t0)